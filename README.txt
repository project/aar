Account Activation Reminder

A module allowing site administrators to resend activation e-mails
from a selected user account page.

 It also offers
 - a custom e-mail subject line
 - a custom e-mail body

 This module is safe to use on a production site. Just be sure to only grant
 'administer users' permission to developers.

AUTHOR/MAINTAINER
======================
-Jason Blanda
http://jasonblanda.com
